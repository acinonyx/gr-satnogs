/* -*- c++ -*- */
/*
 * gr-satnogs: SatNOGS GNU Radio Out-Of-Tree Module
 *
 *  Copyright (C) 2022, 2024 Libre Space Foundation <http://libre.space/>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CCSDS_RS_ENCODER_H
#define CCSDS_RS_ENCODER_H

#include <gnuradio/fec/encoder.h>
#include <gnuradio/satnogs/api.h>
#include <gnuradio/satnogs/code/code.h>
#include <string>

namespace gr {
namespace satnogs {
namespace code {

class SATNOGS_API ccsds_rs_encoder : virtual public fec::generic_encoder
{
public:
    /**
     * @brief Error correction capabilities.
     * @note the parity overhead can be calculated as \f$ \p ecc \times 2 \f$
     *
     */
    enum class ecc : size_t { ecc8 = 8UL, ecc16 = 16UL };

    /**
     * @brief Interleaving depth
     *
     */
    enum class interleaving_depth : size_t {
        depth1 = 1,
        depth2 = 2,
        depth3 = 3,
        depth4 = 4,
        depth5 = 5,
        depth8 = 8
    };

    static fec::generic_encoder::sptr make(ecc e, interleaving_depth depth);

    ccsds_rs_encoder(ecc e, interleaving_depth depth);

    double rate() override;

    /*!
     * Returns the input size in items that the encoder object uses
     * to encode a full frame. Often, this number is the number of
     * bits per frame if the input format is unpacked. If the block
     * expects packed bytes, then this value should be the number of
     * bytes (number of bits / 8) per input frame.
     *
     * The child class MUST implement this function.
     */
    int get_input_size() override;

    /*!
     * Returns the output size in items that the encoder object
     * produces after encoding a full frame. Often, this number is
     * the number of bits in the outputted frame if the input format
     * is unpacked. If the block produces packed bytes, then this
     * value should be the number of bytes (number of bits / 8) per
     * frame produced. This value is generally something like
     * R*get_input_size() for a 1/R rate code.
     *
     * The child class MUST implement this function.
     */
    int get_output_size() override;

    /*!
     * Set up a conversion type required to setup the data properly
     * for this encoder. The encoder itself will not implement the
     * conversion and expects an external wrapper (e.g.,
     * fec.extended_encoder) to read this value and "do the right
     * thing" to format the data.
     *
     * The default behavior is 'none', which means no conversion is
     * required. Whatever the get_input_item_size() value returns,
     * the input is expected to conform directly to this. Generally,
     * this means unpacked bytes.
     *
     * If 'pack', the block expects the inputs to be packed
     * bytes. The wrapper should implement a
     * gr::blocks::pack_k_bits_bb(8) block for this.
     *
     * The child class MAY implement this function. If not
     * reimplemented, it returns "none".
     */
    const char* get_input_conversion() override;

    /*!
     * Set up a conversion type required to understand the output
     * style of this encoder. Generally an encoder will produce
     * unpacked bytes with a bit set in the LSB.
     *
     * The default behavior is 'none', which means no conversion is
     * required and the encoder produces unpacked bytes.
     *
     * If 'packed_bits', the block produces packed bits and the
     * wrapper should unpack these (using, for instance,
     * gr::block::unpack_k_bits_bb(8)).
     *
     * The child class MAY implement this function. If not
     * reimplemented, it returns "none".
     */
    const char* get_output_conversion() override;

    /*!
     * Updates the size of the frame to encode.
     *
     * The child class MUST implement this function and interpret
     * how the \p frame_size information affects the block's
     * behavior. It should also provide bounds checks.
     */
    bool set_frame_size(unsigned int frame_size) override;

    void generic_work(fec_input_buffer_type inbuffer, void* outbuffer) override;

private:
    const ecc m_ecc;
    const interleaving_depth m_depth;
    const size_t m_rs_parity;
    const size_t m_max_in_frame_len;
    const size_t m_max_out_frame_len;
    size_t m_frame_len;
};


} // namespace code
} // namespace satnogs
} // namespace gr

#endif // CCSDS_RS_ENCODER_H
