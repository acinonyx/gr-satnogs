/* -*- c++ -*- */
/*
 * gr-satnogs: SatNOGS GNU Radio Out-Of-Tree Module
 *
 *  Copyright (C) 2016,2019,2022 Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef INCLUDED_SATNOGS_DOPPLER_CORRECTION_CC_H
#define INCLUDED_SATNOGS_DOPPLER_CORRECTION_CC_H

#include <gnuradio/satnogs/api.h>
#include <gnuradio/satnogs/doppler_correction.h>
#include <gnuradio/sync_block.h>

namespace gr {
namespace satnogs {

/*!
 * \brief This block corrects the doppler effect between the ground
 * station and the satellite. It takes the input stream in baseband
 * and applies proper corrections to keep the carrier at the desired
 * frequency. To achieve that it uses messages containing the absolute
 * predicted frequency of the satellite from software like Gpredict.
 *
 * \ingroup satnogs
 *
 */
class SATNOGS_API doppler_correction_cc : virtual public gr::sync_block
{
public:
    using sptr = std::shared_ptr<doppler_correction_cc>;

    /**
     * @brief Generic Doppler correction block. The input is the complex signat at
     * baseband from the SDR device.
     *
     * @param doppler a Doppler correction definition
     * @param sampling_rate the sampling rate of the signal
     * @param offset the frequency offset from the actual target frequency. This is very
     * common on SDR receivers to avoid DC spikes at the center frequency. This block can
     * automatically compensate this offset. The offset and the doppler shift are
     * substracted for the final compensation.
     * @param corrections_per_sec the number of the corrections every second that the
     * block should perform
     * @param doppler_tag_name the name of the stream tag indicating the Doppler offset
     * from the center frequency
     * @return sptr
     */
    static sptr make(doppler_correction::sptr doppler,
                     double sampling_rate,
                     double offset = 0.0,
                     size_t corrections_per_sec = 1000,
                     const std::string& doppler_tag_name = "doppler");
};

} // namespace satnogs
} // namespace gr

#endif /* INCLUDED_SATNOGS_DOPPLER_CORRECTION_CC_H */
