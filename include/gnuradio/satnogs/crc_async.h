/* -*- c++ -*- */
/*
 * gr-satnogs: SatNOGS GNU Radio Out-Of-Tree Module
 *
 *  Copyright (C) 2020,2022 Libre Space Foundation <http://libre.space/>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef INCLUDE_SATNOGS_CRC_ASYNC_H_
#define INCLUDE_SATNOGS_CRC_ASYNC_H_


#include <gnuradio/block.h>
#include <gnuradio/satnogs/api.h>
#include <gnuradio/satnogs/crc.h>

namespace gr {
namespace satnogs {

/*!
 * \brief An extended version of the CRC Async block of GNU Radio, supporting a wide range
 * of different CRCs via the CRCpp
 *
 * \ingroup satnogs
 *
 */
class SATNOGS_API crc_async : virtual public gr::block
{
public:
    using sptr = std::shared_ptr<crc_async>;

    /**
     * @brief Creates a CRC check/append block
     *
     * @tparam CRCType the CRC datatyoe
     * @tparam CRCWidth the CRC Width in bits. Currently only lengths multiple of 8 bits
     * are supported
     * @param crc the CRC parameters
     * @param check true if the block should check for the CRC, false if the block should
     * append the CRC
     * @param nbo true if the CRC should be appended in Network Byte Order (big endian),
     * false for little endian
     * @return sptr a shared pointer to the block instance
     */
    template <typename CRCType, crcpp_uint16 CRCWidth>
    static sptr make(const CRC::Parameters<CRCType, CRCWidth>& crc,
                     bool check = false,
                     bool nbo = true);

    /**
     * @brief Creates a CRC check/append block
     *
     * @param crc the CRC type
     * @param check true if the block should check for the CRC, false if the block should
     * append the CRC
     * @param nbo true if the CRC should be appended in Network Byte Order (big endian),
     * false for little endian
     * @return sptr a shared pointer to the block instance
     */
    static sptr make(crc::type crc, bool check = false, bool nbo = true);
};


} // namespace satnogs
} // namespace gr


#endif /* INCLUDE_SATNOGS_CRC_ASYNC_H_ */
