# * Try to find png++

find_package(PNG REQUIRED)

include(FindPackageHandleStandardArgs)

find_path(
  png++_INCLUDE_DIR
  NAMES png++/png.hpp
  PATHS /usr/local/include /usr/include)

find_package_handle_standard_args(png++ DEFAULT_MSG png++_INCLUDE_DIR)
