#
#  gr-satnogs: SatNOGS GNU Radio Out-Of-Tree Module
#
#  Copyright (C) 2023
#  Libre Space Foundation <http://libre.space>
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>
#
#
# SPDX-License-Identifier: GPL-3.0-or-later
#

#!/bin/bash

# Usage: rigctl_scenario.sh <target-freq> <test-duration in seconds>

FREQ=$1
sleep 2
rigctl -m 2 F ${FREQ}

for (( i=0; c<$2; c++ ))
do
	sleep 1
	FREQ=$((FREQ+1000))
    rigctl -m 2 F ${FREQ}
done
