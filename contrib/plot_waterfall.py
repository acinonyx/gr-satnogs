# /usr/bin/python3

import getopt
import sys

import matplotlib
import matplotlib.pyplot as plt
import numpy as np

matplotlib.use('Agg')


def main(argv):
    inputfile = ''
    outputfile = ''
    vmin = None
    vmax = None
    try:
        opts, args = getopt.getopt(argv, "hi:o:", ["ifile=", "ofile="])
    except getopt.GetoptError:
        print('plot_waterfall.py -i <inputfile> -o <outputfile>')
        sys.exit(1)

    if(len(opts) < 2):
        print('plot_waterfall.py -i <inputfile> -o <outputfile>')
        sys.exit(1)

    for opt, arg in opts:
        if opt == '-h':
            print('plot_waterfall.py -i <inputfile> -o <outputfile>')
            sys.exit(0)
        elif opt in ("-i", "--ifile"):
            inputfile = arg
        elif opt in ("-o", "--ofile"):
            outputfile = arg

    datafile = open(inputfile, mode='rb')
    waterfall = {
        'timestamp': np.fromfile(datafile, dtype='|S32', count=1)[0],
        'nchan': np.fromfile(datafile, dtype='>i4', count=1)[0],
        'samp_rate': np.fromfile(datafile, dtype='>i4', count=1)[0],
        'nfft_per_row': np.fromfile(datafile, dtype='>i4', count=1)[0],
        'center_freq': np.fromfile(datafile, dtype='>f4', count=1)[0],
        'endianess': np.fromfile(datafile, dtype='>i4', count=1)[0]
    }

    print('Sampling Rate: ' + str(waterfall['samp_rate']))
    print(waterfall)

    data_dtypes = np.dtype(
        [('tabs', 'int64'), ('spec', 'float32', (waterfall['nchan'], ))])
    waterfall['data'] = np.fromfile(datafile, dtype=data_dtypes)
    if waterfall['data'].size == 0:
        raise RuntimeError

    nint = waterfall['data']['spec'].shape[0]
    waterfall['trel'] = np.arange(nint) * waterfall['nfft_per_row'] * waterfall['nchan'] / float(
        waterfall['samp_rate'])
    waterfall['freq'] = np.linspace(-0.5 * waterfall['samp_rate'],
                                    0.5 * waterfall['samp_rate'],
                                    waterfall['nchan'],
                                    endpoint=False)

    datafile.close()

    tmin = np.min(waterfall['data']['tabs'] / 1000000.0)
    tmax = np.max(waterfall['data']['tabs'] / 1000000.0)
    fmin = np.min(waterfall['freq'] / 1000.0)
    fmax = np.max(waterfall['freq'] / 1000.0)
    if vmin is None or vmax is None:
        vmin = -100
        vmax = -50
        c_idx = waterfall['data']['spec'] > -200.0
        if np.sum(c_idx) > 100:
            data_mean = np.mean(waterfall['data']['spec'][c_idx])
            data_std = np.std(waterfall['data']['spec'][c_idx])
            vmin = data_mean - 2.0 * data_std
            vmax = data_mean + 6.0 * data_std
    plt.figure(figsize=(10, 20))
    plt.imshow(waterfall['data']['spec'],
               origin='lower',
               aspect='auto',
               interpolation='None',
               extent=[fmin, fmax, tmin, tmax],
               vmin=vmin,
               vmax=vmax,
               cmap='viridis')
    plt.xlabel('Frequency (kHz)')
    plt.ylabel('Time (seconds)')
    fig = plt.colorbar(aspect=50)
    fig.set_label('Power (dB)')
    plt.savefig(outputfile, bbox_inches='tight')
    plt.close()


if __name__ == "__main__":
    main(sys.argv[1:])
