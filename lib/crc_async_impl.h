/* -*- c++ -*- */
/*
 * gr-satnogs: SatNOGS GNU Radio Out-Of-Tree Module
 *
 *  Copyright (C) 2020,2022 Libre Space Foundation <http://libre.space/>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LIB_CRC_ASYNC_IMPL_H_
#define LIB_CRC_ASYNC_IMPL_H_

#include <gnuradio/satnogs/crc_async.h>

namespace gr {
namespace satnogs {

class crc_async_impl : public crc_async
{

public:
    template <typename CRCType, crcpp_uint16 CRCWidth>
    crc_async_impl(const CRC::Parameters<CRCType, CRCWidth>& crc, bool check, bool nbo);

    crc_async_impl(crc::type crc, bool check, bool nbo);

    ~crc_async_impl();

    int general_work(int noutput_items,
                     gr_vector_int& ninput_items,
                     gr_vector_const_void_star& input_items,
                     gr_vector_void_star& output_items) override;

private:
    const bool m_nbo;

    template <typename CRCType, crcpp_uint16 CRCWidth>
    void append(const CRC::Parameters<CRCType, CRCWidth>& crc, pmt::pmt_t msg);

    template <typename CRCType, crcpp_uint16 CRCWidth>
    void check(const CRC::Parameters<CRCType, CRCWidth>& crc, pmt::pmt_t msg);

    void append(crc::type crc, pmt::pmt_t msg);

    void check(crc::type crc, pmt::pmt_t msg);
};

} // namespace satnogs
} // namespace gr

#endif /* LIB_CRC_ASYNC_IMPL_H_ */
