/* -*- c++ -*- */
/*
 * gr-satnogs: SatNOGS GNU Radio Out-Of-Tree Module
 *
 *  Copyright (C) 2017,2022 Libre Space Foundation <http://libre.space/>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef INCLUDED_SATNOGS_OGG_SOURCE_IMPL_H
#define INCLUDED_SATNOGS_OGG_SOURCE_IMPL_H

#include <gnuradio/satnogs/ogg_source.h>
#include <volk/volk.h>
#include <volk/volk_alloc.hh>
#include <vorbis/codec.h>
#include <vorbis/vorbisfile.h>

namespace gr {
namespace satnogs {

class ogg_source_impl : public ogg_source
{
private:
    static constexpr size_t pcm_buffer_size = 4096;
    const int m_channels;
    const bool m_repeat;
    volk::vector<int16_t> m_in_buffer;
    volk::vector<float> m_out_buffer;
    OggVorbis_File m_ogvorb_f;

public:
    ogg_source_impl(const std::string& filename, int channels, bool repeat);
    ~ogg_source_impl();

    // Where all the action really happens
    int work(int noutput_items,
             gr_vector_const_void_star& input_items,
             gr_vector_void_star& output_items);
};

} // namespace satnogs
} // namespace gr

#endif /* INCLUDED_SATNOGS_OGG_SOURCE_IMPL_H */
