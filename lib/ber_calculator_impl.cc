/* -*- c++ -*- */
/*
 * gr-satnogs: SatNOGS GNU Radio Out-Of-Tree Module
 *
 *  Copyright (C) 2020,2022 Libre Space Foundation <http://libre.space/>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "ber_calculator_impl.h"
#include <gnuradio/satnogs/metadata.h>
#include <CRC.h>
#include <cstring>
#include <random>
#include <thread>

namespace gr {
namespace satnogs {

/**
 * Creates the BER calculator block
 * @param frame_size the frame size
 * @param nframes the number of frames to send for the test. If set to 0 the
 * test continues until the user terminates the flowgraph
 * @param skip skip the given number frames at the start of the experiment.
 * Useful to deal with buffering and alignment issues
 * @return shared pointer to a BER calculator block
 */
ber_calculator::sptr ber_calculator::make(size_t frame_size, size_t nframes, size_t skip)
{
    return gnuradio::get_initial_sptr(new ber_calculator_impl(frame_size, nframes, skip));
}

ber_calculator_impl::ber_calculator_impl(size_t frame_size, size_t nframes, size_t skip)
    : gr::block("ber_calculator",
                gr::io_signature::make(0, 0, 0),
                gr::io_signature::make(0, 0, 0)),
      m_frame_size(frame_size),
      m_nframes(nframes),
      m_skip(skip),
      m_cnt(0),
      m_inval_cnt(0),
      m_last_ack(0),
      m_dropped(0),
      m_received(0)
{
    if (frame_size < 8) {
        d_logger->fatal("Minimum supported frame size is 8 bytes");
        throw std::invalid_argument("Minimum supported frame size is 8 bytes");
    }

    message_port_register_in(pmt::mp("trigger"));
    message_port_register_in(pmt::mp("received"));
    message_port_register_out(pmt::mp("pdu"));

    set_msg_handler(pmt::mp("trigger"),
                    [this](pmt::pmt_t msg) { this->create_pdu(msg); });

    set_msg_handler(pmt::mp("received"),
                    [this](pmt::pmt_t msg) { this->received_pdu(msg); });
}

ber_calculator_impl::~ber_calculator_impl() {}

bool ber_calculator_impl::stop()
{
    print_stats();
    /* FIXME: GNU Radio does not exit, if only message passing blocks are used */
    exit(EXIT_SUCCESS);
    return true;
}

/**
 * Appends an PDU that was sent from the transmitter at the DB.
 * @param msg a PMT pair with metadata and PDU
 */
void ber_calculator_impl::create_pdu(pmt::pmt_t msg)
{
    std::random_device rd;
    std::mt19937 mt(rd());
    std::uniform_int_distribution<uint8_t> uni(0, 0xFF);
    std::vector<uint8_t> d(m_frame_size);
    std::generate(d.begin(), d.end(), [&] { return uni(mt); });

    pmt::pmt_t meta(pmt::car(msg));

    memcpy(d.data(), &m_cnt, sizeof(uint64_t));
    auto crc = CRC::Calculate(d.data(), m_frame_size - 4, CRC::CRC_32_BZIP2());
    memcpy(d.data() + m_frame_size - 4, &crc, 4);
    m_cnt++;


    message_port_pub(pmt::mp("pdu"),
                     pmt::cons(meta, pmt::init_u8vector(m_frame_size, d)));

    if (m_nframes != 0 && m_cnt == m_nframes) {
        system_handler(pmt::cons(pmt::intern("done"), pmt::mp(true)));
    }
}

/**
 * Appends an PDU that was sent from the transmitter at the DB
 * @param msg a PMT pair with metadata and PDU
 */
void ber_calculator_impl::received_pdu(pmt::pmt_t msg)
{
    /* Check if the message contains the legacy or the gr-satnogs format */
    pmt::pmt_t bytes;
    if (pmt::is_dict(msg)) {
        bytes = pmt::dict_ref(msg, pmt::mp(metadata::value(metadata::PDU)), pmt::PMT_NIL);
    } else {
        bytes = pmt::cdr(msg);
    }

    size_t pkt_len(0);
    const uint8_t* bytes_in = pmt::u8vector_elements(bytes, pkt_len);
    if (pkt_len != m_frame_size) {
        m_inval_cnt++;
        return;
    }

    auto crc_calc = CRC::Calculate(bytes_in, m_frame_size - 4, CRC::CRC_32_BZIP2());
    uint32_t crc_recv = 0;
    memcpy(&crc_recv, bytes_in + m_frame_size - 4, 4);
    if (crc_recv != crc_calc) {
        m_inval_cnt++;
        return;
    }

    uint64_t cnt = 0;
    memcpy(&cnt, bytes_in, sizeof(uint64_t));
    if (cnt < m_last_ack || cnt > m_cnt) {
        m_inval_cnt++;
        return;
    }

    if (cnt < m_skip) {
        m_last_ack = cnt + 1;
        return;
    }

    m_dropped += (cnt - m_last_ack);
    m_last_ack = cnt + 1;
    m_received++;
}

/**
 *
 * @return the FER (Frame Error Rate).
 * @note the FER does not take into consideration the last unacknowledged frames,
 * to mitigate buffering issues inside the processing chain
 */
double ber_calculator_impl::fer()
{
    if (m_last_ack == 0) {
        return 1.0;
    }
    return (double)m_dropped / m_last_ack;
}

/**
 * Calculates the BER based on the FER
 * The formula is: \f$ FER = 1 - (1 - BER)^{N} \f$ where \f$ N \f$ is the number
 * of bits of the frame. Assuming that the preamble, SFD are not affecting a lot
 * the result the BER can be calculated as:
 * \f$ BER = 1 - 10^{\frac{log_{10}{1-FER}}{N}}\f$
 * @return the estimated BER
 */
double ber_calculator_impl::ber()
{
    return 1.0 - std::pow(10.0, std::log10(1.0 - fer()) / m_frame_size * 8);
}

void ber_calculator_impl::print_stats()
{
    std::cout << "Frames sent     : " << m_cnt << std::endl;
    std::cout << "Frames received : " << m_received << std::endl;
    std::cout << "Frames lost     : " << m_dropped << std::endl;
    std::cout << "Frames invalid  : " << m_inval_cnt << std::endl;
    std::cout << "FER             : " << fer() << std::endl;
    std::cout << "BER estimation  : " << ber() << std::endl;
}

int ber_calculator_impl::general_work(int noutput_items,
                                      gr_vector_int& ninput_items,
                                      gr_vector_const_void_star& input_items,
                                      gr_vector_void_star& output_items)
{
    if (m_nframes != 0 && m_cnt == m_nframes) {
        return WORK_DONE;
    }
    return 0;
}

} /* namespace satnogs */
} /* namespace gr */
