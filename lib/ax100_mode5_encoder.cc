/* -*- c++ -*- */
/*
 *  gr-satnogs: SatNOGS GNU Radio Out-Of-Tree Module
 *
 *  Copyright (C) 2021-2023, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  SPDX-License-Identifier: GNU General Public License v3.0 or later
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <gnuradio/satnogs/ax100_mode5_encoder.h>
#include <gnuradio/satnogs/golay24.h>
#include <gnuradio/satnogs/libfec/fec.h>
#include <algorithm>

namespace gr {
namespace satnogs {

ax100_mode5_encoder::sptr ax100_mode5_encoder::make(const std::vector<uint8_t>& preamble,
                                                    const std::vector<uint8_t>& sync,
                                                    crc::type crc,
                                                    whitening::sptr scrambler,
                                                    bool enable_rs)
{
    return ax100_mode5_encoder::sptr(
        new ax100_mode5_encoder(preamble, sync, crc, scrambler, enable_rs));
}

ax100_mode5_encoder::ax100_mode5_encoder(const std::vector<uint8_t>& preamble,
                                         const std::vector<uint8_t>& sync,
                                         crc::type crc,
                                         whitening::sptr scrambler,
                                         bool enable_rs)
    : d_crc(crc),
      d_rs(enable_rs),
      d_payload_start(preamble.size() + sync.size()),
      d_scrambler(scrambler)
{
    if (!d_scrambler) {
        throw std::invalid_argument("Invalid scrambling object");
    }

    if (sync.size() * 8 < 8) {
        throw std::invalid_argument("SYNC word should be at least 8 bits");
    }

    d_pdu =
        new uint8_t[preamble.size() + sync.size() + length_size + 255 + crc::size(d_crc)];

    std::copy(preamble.begin(), preamble.end(), d_pdu);
    std::copy(sync.begin(), sync.end(), d_pdu + preamble.size());
}

ax100_mode5_encoder::~ax100_mode5_encoder() { delete[] d_pdu; }

pmt::pmt_t ax100_mode5_encoder::encode(pmt::pmt_t msg)
{
    pmt::pmt_t b;
    if (pmt::is_blob(msg)) {
        b = msg;
    } else if (pmt::is_pair(msg)) {
        b = pmt::cdr(msg);
    } else {
        throw std::runtime_error("ax100_mode5_encoder: received a malformed pdu message");
    }

    size_t pdu_len(0);
    const uint8_t* pdu = (const uint8_t*)pmt::uniform_vector_elements(b, pdu_len);
    if (pdu_len > 223 - crc::size(d_crc)) {
        throw std::runtime_error("ax100_mode5_encoder: PDU received has a size larger "
                                 "than the maximum allowed");
    }

    size_t overhead = d_rs ? 32 : 0;
    golay24 g = golay24();
    uint32_t enc_len =
        g.encode12((uint16_t)(pdu_len + overhead + crc::size(d_crc)), false);

    /* Append the encoded length field */
    size_t idx = d_payload_start;
    d_pdu[idx++] = enc_len >> 16;
    d_pdu[idx++] = enc_len >> 8;
    d_pdu[idx++] = enc_len;

    std::copy_n(pdu, pdu_len, d_pdu + idx);
    idx += pdu_len;

    overhead +=
        crc::append(d_crc, d_pdu + idx, d_pdu + d_payload_start + length_size, pdu_len);
    idx += crc::size(d_crc);
    if (d_rs) {
        encode_rs_8(d_pdu + d_payload_start + length_size,
                    d_pdu + idx,
                    223 - pdu_len - crc::size(d_crc));
    }

    d_scrambler->reset();
    d_scrambler->scramble(d_pdu + d_payload_start + length_size,
                          d_pdu + d_payload_start + length_size,
                          pdu_len + overhead);

    pmt::pmt_t vecpmt(
        pmt::make_blob(d_pdu, d_payload_start + length_size + pdu_len + overhead));
    pmt::pmt_t res(pmt::cons(pmt::PMT_NIL, vecpmt));
    return res;
}

} /* namespace satnogs */
} /* namespace gr */
