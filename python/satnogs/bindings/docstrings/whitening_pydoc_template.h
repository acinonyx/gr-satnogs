/*
 * Copyright 2023 Free Software Foundation, Inc.
 *
 * This file is part of GNU Radio
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 */
#include "pydoc_macros.h"
#define D(...) DOC(gr, satnogs, __VA_ARGS__)
/*
  This file contains placeholders for docstrings for the Python bindings.
  Do not edit! These were automatically extracted during the binding process
  and will be overwritten during the build process
 */


static const char* __doc_gr_satnogs_whitening = R"doc()doc";


static const char* __doc_gr_satnogs_whitening_whitening_0 = R"doc()doc";


static const char* __doc_gr_satnogs_whitening_whitening_1 = R"doc()doc";


static const char* __doc_gr_satnogs_whitening_unique_id = R"doc()doc";


static const char* __doc_gr_satnogs_whitening_make = R"doc()doc";


static const char* __doc_gr_satnogs_whitening_make_ccsds = R"doc()doc";


static const char* __doc_gr_satnogs_whitening_make_g3ruh = R"doc()doc";


static const char* __doc_gr_satnogs_whitening_make_none = R"doc()doc";


static const char* __doc_gr_satnogs_whitening_reset = R"doc()doc";


static const char* __doc_gr_satnogs_whitening_scramble = R"doc()doc";


static const char* __doc_gr_satnogs_whitening_descramble = R"doc()doc";


static const char* __doc_gr_satnogs_whitening_scramble_one_bit_per_byte = R"doc()doc";


static const char* __doc_gr_satnogs_whitening_descramble_one_bit_per_byte = R"doc()doc";
