/*
 * Copyright 2023 Free Software Foundation, Inc.
 *
 * This file is part of GNU Radio
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 */
#include "pydoc_macros.h"
#define D(...) DOC(gr, satnogs, __VA_ARGS__)
/*
  This file contains placeholders for docstrings for the Python bindings.
  Do not edit! These were automatically extracted during the binding process
  and will be overwritten during the build process
 */


static const char* __doc_gr_satnogs_crc = R"doc()doc";


static const char* __doc_gr_satnogs_crc_crc_0 = R"doc()doc";


static const char* __doc_gr_satnogs_crc_crc_1 = R"doc()doc";


static const char* __doc_gr_satnogs_crc_crc16_ccitt_reversed = R"doc()doc";


static const char* __doc_gr_satnogs_crc_crc16_ccitt = R"doc()doc";


static const char* __doc_gr_satnogs_crc_crc16_aug_ccitt = R"doc()doc";


static const char* __doc_gr_satnogs_crc_crc16_ax25 = R"doc()doc";


static const char* __doc_gr_satnogs_crc_crc16_ibm = R"doc()doc";


static const char* __doc_gr_satnogs_crc_crc32_c = R"doc()doc";


static const char* __doc_gr_satnogs_crc_crc_size = R"doc()doc";


static const char* __doc_gr_satnogs_crc_append = R"doc()doc";


static const char* __doc_gr_satnogs_crc_check = R"doc()doc";


static const char* __doc_gr_satnogs_crc_size = R"doc()doc";
