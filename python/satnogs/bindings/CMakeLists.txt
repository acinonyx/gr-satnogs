# Copyright 2020 Free Software Foundation, Inc.
#
# This file is part of GNU Radio
#
# SPDX-License-Identifier: GPL-3.0-or-later
#

# ##############################################################################
# Check if there is C++ code at all
# ##############################################################################
if(NOT satnogs_sources)
  message(STATUS "No C++ sources... skipping python bindings")
  return()
endif(NOT satnogs_sources)

# ##############################################################################
# Check for pygccxml
# ##############################################################################
gr_python_check_module_raw("pygccxml" "import pygccxml" PYGCCXML_FOUND)

include(GrPybind)

# ##############################################################################
# Python Bindings
# ##############################################################################

list(
  APPEND
  satnogs_python_files
  amsat_duv_decoder_python.cc
  argos_ldr_decoder_python.cc
  ax100_decoder_python.cc
  ax100_encoder_python.cc
  ax25_encoder_python.cc
  ax25_decoder_python.cc
  ber_calculator_python.cc
  crc_async_python.cc
  crc_python.cc
  cw_decoder_python.cc
  cw_encoder_python.cc
  doppler_correction_cc_python.cc
  doppler_correction_python.cc
  dummy_doppler_correction_python.cc
  frame_decoder_python.cc
  frame_encoder_python.cc
  ieee802_15_4_variant_decoder_python.cc
  ieee802_15_4_encoder_python.cc
  json_converter_python.cc
  metadata_sink_python.cc
  noaa_apt_sink_python.cc
  ogg_encoder_python.cc
  ogg_source_python.cc
  rigctl_doppler_correction_python.cc
  sigmf_metadata_python.cc
  sstv_pd120_sink_python.cc
  waterfall_sink_python.cc
  whitening_python.cc
  python_bindings.cc)

gr_pybind_make_oot(satnogs ../../.. gr::satnogs "${satnogs_python_files}")

# copy bindings extension for use in QA test module
add_custom_command(
  TARGET satnogs_python
  POST_BUILD
  COMMAND ${CMAKE_COMMAND} -E copy $<TARGET_FILE:satnogs_python>
          ${PROJECT_BINARY_DIR}/test_modules/gnuradio/satnogs/)

install(
  TARGETS satnogs_python
  DESTINATION ${GR_PYTHON_DIR}/gnuradio/satnogs
  COMPONENT pythonapi)
